package examplesoureit.colorview;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import butterknife.BindArray;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.first_view)
    View firstView;
    @BindView(R.id.second_view)
    View secondView;
    @BindView(R.id.third_view)
    View thirdView;
    @BindView(R.id.forth_view)
    View fourthView;
    //    @BindArray(R.array.color_array)
//    protected String[] colorList;
//
    List<String> colorList = new ArrayList<>();

    List<View> viewList = new ArrayList();

    Random random;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);


        random = new Random();

        generateViewsList();
        generateColorList();


//        View.OnClickListener onClickListener = new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                for (String s : colorList) {
//                    System.out.println(s);
//                }
//
////                System.out.println(colorList.length);
////                for (int s : colorList) {
////                    System.out.println(s);
////                }
////                for (View view : viewList) {
////                    int r = random.nextInt();
////                    if (v.getId() != view.getId()) {
////                        random.nextInt();
////                        int randomColor = Color.parseColor(colorList.get(random.nextInt(10)));
////                        boolean verifyColor = false;
////                        while(verifyColor){
////                            randomColor = Color.parseColor(colorList.get(random.nextInt(10)));
////                            verifyColor = true;
////                        }
////                        v.setBackgroundColor(randomColor);
////
////
////                    }
////                }
//            }
//        };


//        firstView.setOnClickListener(onClickListener);
//        secondView.setOnClickListener(onClickListener);
//        thirdView.setOnClickListener(onClickListener);
//        fourthView.setOnClickListener(onClickListener);

    }

    @OnClick({R.id.first_view, R.id.second_view, R.id.third_view, R.id.forth_view})
    public void onClickview(View v) {
        for (View view : viewList) {
            if (view.getId() != v.getId()) {
                String randomColor = colorList.get(random.nextInt(10));

                boolean verifyViewColor = false;
                while (!verifyViewColor) {
                    if (firstView.getBackground().toString() != randomColor &&
                            secondView.getBackground().toString() != randomColor &&
                            thirdView.getBackground().toString() != randomColor &&
                            fourthView.getBackground().toString() != randomColor) {
                        System.out.println(((ColorDrawable)firstView.getBackground()).getColor() + "--- "+ Color.parseColor(randomColor));
                        view.setBackgroundColor(Color.parseColor(randomColor));
                        verifyViewColor = true;
                    }
                    else {
                        randomColor = colorList.get(random.nextInt(10));
                    }
                }

                System.out.println(verifyViewColor);
                view.setBackgroundColor(Color.parseColor(randomColor));


            }
        }
    }

//    private void setColor(int id) {
//
//        Random random = new Random(11);
////        if (item)
//
//    }

    private void generateViewsList() {
        viewList.add(firstView);
        viewList.add(secondView);
        viewList.add(thirdView);
        viewList.add(fourthView);
    }

    private void generateColorList() {
        colorList.add("#90EE90");
        colorList.add("#FFB6C1");
        colorList.add("#7B68EE");
        colorList.add("#808000");
        colorList.add("#87CEEB");
        colorList.add("#9ACD32");
        colorList.add("#FF6347");
        colorList.add("#FFFAFA");
        colorList.add("#BC8F8F");
        colorList.add("#FFDAB9");


    }
}
